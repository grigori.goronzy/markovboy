import os
import re
import cherrypy
import time
from string import Template
import argparse
import random
import markovify
import shelve
import binascii
import nltk

def no_index():
    """Tool to disable slash redirect for indexes"""
    cherrypy.request.is_index = False
cherrypy.tools.no_index = cherrypy.Tool('before_handler', no_index)

def restrict_methods(methods):
    """Tool for restricting to a certain set of allowed HTTP methods"""
    if not cherrypy.request.method in methods:
        raise cherrypy.HTTPError(405)
cherrypy.tools.restrict_methods = cherrypy.Tool('before_handler', restrict_methods)


class POSifiedText(markovify.NewlineText):
    def word_split(self, sentence):
        words = re.split(self.word_split_pattern, sentence)
        words = [ "::".join(tag) for tag in nltk.pos_tag(words) ]
        return words

    def word_join(self, words):
        sentence = " ".join(word.split("::")[0] for word in words)
        return sentence


@cherrypy.popargs("rap_id")
class Rap:
    def __init__(self, model, db):
        self._model = model
        self._db = db
        self._view_template = Template(open("view.template").read())
    
    def _generate_rap(self):
        """Generate a basic rap sequence"""
        n_parts = random.randint(2, 3)
        n_len = random.randint(4, 10)
        rap = []
        for _ in range(n_parts):
            for _ in range(n_len):
                rap.append(self._model.make_sentence(tries=1000))
            rap.append("")
        return rap

    @cherrypy.expose()
    @cherrypy.tools.no_index()
    @cherrypy.tools.restrict_methods(methods = ["GET"])
    def index(self, rap_id=None):
        if rap_id is None:
            self._new_rap()
        else:
            return self._view_rap(rap_id)

    def _view_rap(self, rap_id):
        """View a generated rap"""
        if rap_id in self._db:
            rap_pre = "\n".join(self._db[rap_id])
            num_bg = ord(rap_id[1]) % 3
            return self._view_template.substitute(rap=rap_pre, bgn=num_bg)
        else:
            raise cherrypy.HTTPError(404)

    def _new_rap(self):
        """Generate a new rap sequence, store in database and redirect to it"""
        rap_key = binascii.b2a_hex(os.urandom(15)).decode("ASCII")
        rap_text = self._generate_rap()
        print(rap_key, rap_text)
        self._db[rap_key] = rap_text
        raise cherrypy.HTTPRedirect("/rap/{}".format(rap_key))
    


class Root:
    def __init__(self, model, db):
        self.rap = Rap(model, db)

    @cherrypy.expose()
    @cherrypy.tools.no_index()
    @cherrypy.tools.restrict_methods(methods = ["GET"])
    def index(self):
        raise cherrypy.HTTPRedirect("/rap")

def init_markov_model():
    """Initialize Markov model from lyrics database"""
    with open("mb.txt") as f:
        text = f.read()
    text_model = POSifiedText(text, state_size=2)
    return text_model

def start_web(model, db):
    """Start web server"""
    parser = argparse.ArgumentParser(description="mb")
    parser.add_argument("--debug", "-d", action="store_true", help="debug mode")
    args = parser.parse_args()

    root = Root(model, db)

    global_config = {
        'server.socket_host': "0.0.0.0",
        'server.socket_port': 1234,
        'tools.proxy.on': True,
    }

    app_config = {
        '/': {
            'tools.staticdir.root': os.path.abspath(os.getcwd()),
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './static',
            'tools.staticdir.index': 'index.html',
        },
    }
    
    if args.debug == False:
        cherrypy.config.update(cherrypy.config.environments["production"])

    cherrypy.config.update(global_config)
    cherrypy.quickstart(root, '/', app_config)

if __name__ == "__main__":
    with shelve.open('./data/rap.db') as db:
        text_model = init_markov_model()
        start_web(text_model, db)