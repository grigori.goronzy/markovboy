markovboy
=========

markovboy is random rap lyrics generator. It utilizes markovify and natural
language processing to generate random texts, which are displayed on a
website.
