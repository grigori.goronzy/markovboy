FROM python:3.6.5-alpine3.7

EXPOSE 1234

VOLUME /data

COPY mb.py mb.txt view.template requirements.txt /
COPY static/* /static/

RUN pip3 install -r requirements.txt && \
    python3 -m nltk.downloader "averaged_perceptron_tagger"

CMD ["python3", "mb.py"]